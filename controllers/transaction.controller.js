const { Validator } = require('node-input-validator');
const connection = require('../db/connection');

module.exports = {
    get: async function(req,res, next){

        const [total,fields1] = await connection.execute('SELECT SUM(amount) AS total,type FROM transactions GROUP BY type');
        
        let final = 0;
        for(i=0;i<total.length;i++){
            if(total[i].type == "C"){
                final += total[i].total; 
            }
            if(total[i].type == "D"){
                final -= total[i].total; 
            }
        }

        const [transaction,fields] = await connection.execute('SELECT * FROM `transactions` ORDER BY `id` DESC');

        let totalamount = 0;

        if(total.length>0){
            totalamount = total[0].total;
        }
        return res.render('index',{transaction,final});
    },
    add: async function(req, res, next){
        return res.render('add');
    },
    insert: async function(req, res, next){

        const body = req.body;

        const v = new Validator(req.body, {
            amount: 'required',
            description: 'required'
        });

        const matched = await v.check();

        if(!matched){
            return res.json({
                status: 201,
                message: v.errors
            })
        }

        const [total,fields1] = await connection.execute('SELECT SUM(amount) AS total,type FROM transactions GROUP BY type');
        
        let final = 0;
        for(i=0;i<total.length;i++){
            if(total[i].type == "C"){
                final += total[i].total; 
            }
            if(total[i].type == "D"){
                final -= total[i].total; 
            }
        }

        let finalrunning = final;

        if(body.type == "C"){
            finalrunning = parseFloat(final)+parseFloat(body.amount);
        }else if(body.type == "D"){
            finalrunning = parseFloat(final)-parseFloat(body.amount);
        }


        const [insert,fields] = await connection.execute('INSERT INTO `transactions`(`type`,`amount`,`description`,`running_balance`) VALUES(?,?,?,?)',[body.type,body.amount,body.description,finalrunning]);

        return res.redirect('/');
        
    }
}