const mysql = require('mysql2/promise');
const pool = mysql.createPool({
  host:'127.0.1.1',
  user: 'root',
  password: '',
  database: 'transaction',
  waitForConnections: true,
  connectionLimit: 1000,
  queueLimit: 0
});




module.exports = pool;
