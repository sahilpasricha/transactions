var express = require('express');
var router = express.Router();
var transactionController = require('../controllers/transaction.controller');

/* GET home page. */
router.get('/', transactionController.get);
router.get('/add',transactionController.add);
router.post('/insert',transactionController.insert);

module.exports = router;
